# WhatsApp Web Session Hijacking

Python script and HTML website for hijacking WhatsApp Web sessions.

## How it is work?
The script opens the WhatsApp website and updates the QR code every 18 seconds. 
The saved QR code uploaded to the FAKE website and when someone scans the QR code 
the Whatsapp web session opened in your browser.
The QR code updates every 5 seconds on the website.

Has been done with selenium-webdriver.

## How to use it?
### For Self-Testing:
Just run the script `WhatsApp_Session_Hijacking.py` (*python3*), open the FAKE website and try it!
### For Real HACKING:
You can set-up a local server for the fake website, and poison the DNS cache at the router/target computer.

[](url)https://youtu.be/ZwVzenNXMxI